const express = require('express')
const router = express.Router()
const userController = require('../controllers/user.js')
const auth = require('../auth.js')
const Course = require('../models/Course.js')

// route for checking if email already exist
router.post('/checkEmail', (req, res) => {
    userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
})

// route for registering a user
router.post('/register', (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// route for user login
router.post('/login', (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Activity
router.post('/details', (req, res) => {
    userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
})

// Personal Challenge
router.get('/details', auth.verify, (req, res) => {
    userController.getProfileByAuth(auth.decode(req.headers.authorization)).then(resultFromController => res.send(resultFromController))
})

// Enroll user to a course
router.get("/details", auth.verify, (req, res) => {
	auth.decode(req.headers.authorization)

	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
})

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : req.body.id,
		courseId : req.body.courseId
	}

	const token = auth.decode(req.headers.authorization)
    if(token.isAdmin){
        res.send('You need to be a student to enroll')
    } else {
        userController.enroll(data).then(resultFromController => res.send(resultFromController))
    }
})


module.exports = router