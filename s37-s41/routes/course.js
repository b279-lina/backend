const express = require('express')
const router = express.Router()
const courseController = require('../controllers/course.js')
const auth = require('../auth.js')

// Route for creating a course
router.post('/', auth.verify, (req, res) => {
    const reqBody = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    
    courseController.addCourse(reqBody).then(resultFromController => res.send(resultFromController))

})

// Get all courses
router.get('/all', (req, res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// Get all "ACTIVE" courses
router.get('/', (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})

// Update specific course
router.get('/:courseId', (req, res) => {
    console.log(req.params.courseId)
    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Retrieve specific course
router.put('/:courseId', auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    courseController.updateCourse(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController))
})

router.put('/:courseId/archive', auth.verify, (req, res) => {
    if(auth.decode(req.headers.authorization).isAdmin !== true){
        res.send(`You don't have the rights for this action`)
    } else {
        courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
    }
})

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router