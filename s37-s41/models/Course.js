const mongoose = require('mongoose')
const courseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, `Course NAME is required!`]
    },
    description: {
        type: String,
        required: [true, `Course DESCRIPTION is required!`]
    },
    price: {
        type: Number,
        required: [true, `Course PRICE is required!`]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        // The "new Date" expression instantiates the current date
        default: new Date()
    },
    enrollees: [
        {
            userId: {
                type: String,
                required: [true, `USERID is required!`]
            },
            erolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
})

module.exports = mongoose.model('course', courseSchema)