// SERVER CREATION AND DB CREATION
const express = require('express')
const mongoose = require('mongoose')
// Allows us to control the app's cross origin 
const cors= require('cors')
const userRoutes = require('./routes/user.js')
const courseRoutes = require('./routes/course.js')
const app = express()


// Middlewares
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// MongoDB Connection using SRV link
mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.fz6dt1w.mongodb.net/Course_Booking_System?retryWrites=true&w=majority', 
{
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
// Optional - Valdation of DB Connection
mongoose.connection.once(`open`, () => console.log(`Now connected to MongoDB Atlas`))

// Defines the "/user" string to be included for the user routes defined in the "user.js" route file
app.use('/users', userRoutes)
app.use('/courses', courseRoutes)

// PORT LISTENING
if(require.main === module) {
    app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.PORT || 4000}`))
}

module.exports = app