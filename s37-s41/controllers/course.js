const Course = require('../models/Course.js')

// Create a new course
module.exports.addCourse = (reqBody) => {
	console.log(reqBody.isAdmin);
	if(reqBody.isAdmin) {
		let newCourse = new Course({
			name: reqBody.course.name,
			description: reqBody.course.description,
			price: reqBody.course.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return error;
			}
			return course;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}


// retrieve all courses function
module.exports.getAllCourses = () => {
	return Course.find().then(res => {
		return res
	})
}

// retrieve all active courses function
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(res => {
		return res
	})
}

// Retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(res => {
		return res
	})
}

// Update a course
module.exports.updateCourse = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin)
	if(isAdmin){
		let updatedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}
	
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((res, err) =>{
			if(err){
				return false
			} else {
				return `Course Updated!`
			}
		})
	}

	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}

module.exports.archiveCourse = (reqParams, reqBody) => {
	return Course.findByIdAndUpdate(reqParams.courseId, reqBody).then((res, err) => {
		if(err){
			return false
		} else {
			return true
		}
	})
}

// module.exports.archiveCourse = (reqParams, reqBody) => {
// 	let isActive = {
// 		isActive: false
// 	}
// 	return Course.findByIdAndUpdate(reqParams.courseId, isActive).then((res, err) => {
// 		if(err){
// 			return false
// 		} else {
// 			return true
// 		}
// 	})
// }