// Controllers contain the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder

const Task = require('../models/task.js');

module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result;
    })
};

module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        // Sets the name property with the value recieved from the client postman
        name: requestBody.name
    })
    return newTask.save().then((task, err) => {
        if(err){
            console.log(err)
            return false
        } else {
            return task
        }
    })
}

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removeTask, err) => {
        if(err){
            console.log(err)
            return false
        } else {
            return removeTask
        }
    })
}

module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, err) => {
        if(err) {
            console.log(err)
            return false
        }

        result.name = newContent.name
        return result.save().then((updateTask, err) => {
            if (err){
                console.log(err)
                return false
            } else {
                return updateTask
            }
        })
    })
}

module.exports.getSpecific = (taskId) => {
    return Task.findById(taskId).then(result => {
        return result
    })
};

// module.exports.updateStatus = (taskId, status) unsafe due to params can change to anything
module.exports.updateStatus = (taskId, status) => {
    return Task.findById(taskId).then((result, err) => {
        if(err){
            console.log(err)
            return false
        } else {
            // result.status = status (Unsafe due to params can change to anything)
            result.status = 'Complete'
            return result.save().then((updateStatus, err) => {
                if(err){
                    console.log(err)
                    return false
                } else {
                    return updateStatus
                }
            })
        }
    })
}