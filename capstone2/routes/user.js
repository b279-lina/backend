const express = require('express')
const router = express.Router()
const userController = require('../controllers/user')
const auth = require('../auth')

router.post('/register', (req, res) => {
    userController.register(req.body).then(resultFromUserController => res.send(resultFromUserController))
})

router.post('/login', (req, res) => {
    userController.login(req.body).then(resultFromUserController => res.send(resultFromUserController))
})

router.post('/user', auth.verify, (req, res) => {
    userController.userDetails(auth.decode(req.headers.authorization)).then(resultFromUserController => res.send(resultFromUserController))
})

router.put('/users/:userId/makeAdmin', auth.verify, (req, res) => {
    const authenticate = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        id: auth.decode(req.headers.authorization).id
    }

    userController.makeAdmin(req.params, authenticate).then(resultFromUserController => res.send(resultFromUserController))
})

router.put('/users/:userId/removeAdmin', auth.verify, (req, res) => {
    const authenticate = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        id: auth.decode(req.headers.authorization).id
    }

    userController.removeAdmin(req.params, authenticate).then(resultFromUserController => res.send(resultFromUserController))
})

router.post('/allUser', auth.verify, (req, res) => {
    userController.getAllUser(auth.decode(req.headers.authorization).isAdmin).then(resultFromUserController => res.send(resultFromUserController))
})

router.get('/users/:userId', (req, res) => {
    userController.getUserDetails(req.params).then(resultFromController => res.send(resultFromController))
})

module.exports = router