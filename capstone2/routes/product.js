const express = require('express')
const router = express.Router()
const productController = require('../controllers/product')
const auth = require('../auth')

router.post('/create', auth.verify, (req, res) => {
    let data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.createProduct(data).then(resultFromController => res.send(resultFromController))
})

router.post('/allProducts', auth.verify, (req, res) => {
    productController.getAllProduct(auth.decode(req.headers.authorization).isAdmin).then(resultFromController => res.send(resultFromController))
})

router.get('/products', (req, res) => {
    productController.activeProduct().then(resultFromController => res.send(resultFromController))
})

router.get('/products/:productId', (req, res) => {
    productController.getProductById(req.params).then(resultFromController => res.send(resultFromController))
})

router.put('/products/:productId/update', auth.verify, (req, res) => {
    let data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController))
})

router.put('/products/:productId/updateStock', (req, res) => {
    let data = {
        product: req.body
    }

    productController.updateProductStock(req.params, data).then(resultFromController => res.send(resultFromController))
})

router.put('/products/:productId/archive', auth.verify, (req, res) => {
    const isAdmin = {isAdmin: auth.decode(req.headers.authorization).isAdmin}
    productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController))
})

router.put('/products/:productId/unArchive', auth.verify, (req, res) => {
    const isAdmin = {isAdmin: auth.decode(req.headers.authorization).isAdmin}
    productController.unArchiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController))
})

module.exports = router