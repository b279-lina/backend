const express = require('express')
const router = express.Router()
const orderController = require('../controllers/order')
const auth = require('../auth')

router.post('/:productId/checkOut', auth.verify, (req, res) => {
    const data = {
        quantity: req.body,
        id: auth.decode(req.headers.authorization).id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    orderController.checkOut(req.params, data).then(resultFromController => res.send(resultFromController))
})

router.post('/orders', auth.verify, (req, res) => {
    const data = {
        id: auth.decode(req.headers.authorization).id
    }
    orderController.getOrders(data).then(resultFromController => res.send(resultFromController))
})

router.post('/allOrders', auth.verify, (req, res) => {
    orderController.getAllOrders(auth.decode(req.headers.authorization).isAdmin).then(resultFromController => res.send(resultFromController))
})

module.exports = router