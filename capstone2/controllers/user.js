const User = require('../models/Users')
const bcrypt = require('bcrypt')
const auth = require('../auth')

module.exports.register = reqBody => {
    return User.find({email: reqBody.email}).then(res => {
        if(res == ''){
            if(reqBody.email == '' || reqBody.password == ''){
                return {isEmpty: false}
            } else {
                let newUser = new User({
                    email: reqBody.email,
                    password: bcrypt.hashSync(reqBody.password, 10)
                })
                return newUser.save().then((res, err) => {
                    if(err){
                        return err
                    } else {
                        return {registered: `${newUser.email} Successfully registered`}
                    }
                })
            }
        } else {
            return {email: false}
        }
    })
}

module.exports.login = reqBody => {
    return User.findOne({email: reqBody.email}).then(res => {
        if(res){
            const isCorrect = bcrypt.compareSync(reqBody.password, res.password)
            
            if(isCorrect){
                return {token: auth.createAccessToken(res)}
            } else {
                return {password: false}
            }
        } else {
            return {email: false}
        }
    })
}

module.exports.userDetails = reqBody => {
    return User.findOne({_id: reqBody.id}).then(res => {
        if(res == null){
            return Promise.resolve(`Please login`)
        } else {
            res.password = ''
            return res
        }
    })
}

module.exports.makeAdmin = (reqParams, reqBody) => {
    if(reqBody.isAdmin){
        let update = {
            isAdmin: true
        }

        return User.findByIdAndUpdate(reqParams.userId, update).then((res, err) => {
            if(err){
                return `Something went wrong. Please try again later!`
            } else {
                return `User is now an admin`
            }
        })
    } else {
        return Promise.resolve(`Unauthorized access`)
    }
}

module.exports.removeAdmin = (reqParams, reqBody) => {
    if(reqBody.isAdmin){
        let update = {
            isAdmin: false
        }

        return User.findByIdAndUpdate(reqParams.userId, update).then((res, err) => {
            if(err){
                return `Something went wrong. Please try again later!`
            } else {
                return `User is now an admin`
            }
        })
    } else {
        return Promise.resolve(`Unauthorized access`)
    }
}

module.exports.getAllUser = reqBody => {
    if(reqBody){
        return User.find({}).then(res => {
            if(res.length <= 0){
                return `There's no user as of this moment`
            } else {
                return res
            }
        })
    } else {
        return Promise.resolve(`Unauthorized access`)
    }
}

module.exports.getUserDetails = (reqParams) => {
    return User.findById(reqParams.userId).then(res => {
        if(res){
            return res
        } else {
            return false
        }
    })
}