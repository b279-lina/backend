const Product = require('../models/Products')
const bcrypt = require('bcrypt')
auth = require('../auth')

module.exports.createProduct = reqBody => {
    if(reqBody.isAdmin){
        let newProduct = new Product({
            name: reqBody.product.name,
            description: reqBody.product.description,
            price: reqBody.product.price,
            stock: reqBody.product.stock
        })

        return newProduct.save().then((res, err) => {
            if(res){
                return `${res.name} has been created`
            } else {
                return `Something went wrong. Please try again later`
            }
        })
    } else {
        return Promise.resolve(`Unauthorized access`)
    }
}

module.exports.getAllProduct = reqBody => {
    if(reqBody){
        return Product.find({}).then(res => {
            if(res.length <= 0){
                return `There's no product as of this moment`
            } else {
                return res
            }
        })
    } else {
        return Promise.resolve(`Unauthorized access`)
    }
}

module.exports.activeProduct = () => {
    return Product.find({isActive : true}).then(res => {
        if(res == null){
            return `There's no product as of this moment. Please try again later!`
        } else {
            return res
        }
    })
}

module.exports.getProductById = (reqParams) => {
    return Product.findById(reqParams.productId).then(res => {
        if(res){
            return res
        } else {
            return res.status(404).send(`Item not found`)
        }
    })
}

module.exports.updateProduct = (reqParams, reqBody) => {
    if(reqBody.isAdmin){
        const update = {
            name: reqBody.product.name,
            description: reqBody.product.description,
            price: reqBody.product.price,
            stock: reqBody.product.stock
        }

        return Product.findByIdAndUpdate(reqParams.productId, update).then((res, err) => {
            if(err){
                return false
            } else {
                return true
            }
        })
    } else {
        return Promise.resolve(`Unauthorized access`)
    }
}

module.exports.updateProductStock = (reqParams, reqBody) => {
        const update = {
            stock: reqBody.product.stock
        }

        return Product.findByIdAndUpdate(reqParams.productId, update).then((res, err) => {
            if(err){
                return `Unable to update at this time. Please try again later!`
            } else {
                return `Product has been updated`
            }
        })
}

module.exports.archiveProduct = (reqParams, reqBody) => {
    if(reqBody.isAdmin){
        let update = {
            isActive: false
        }

        return Product.findByIdAndUpdate(reqParams.productId, update).then((res, err) => {
            if(err){
                return `Something went wrong. Please try again later!`
            } else {
                return `Product has been successfully archived`
            }
        })
    } else {
        return Promise.resolve(`Unauthorized access`)
    }
}

module.exports.unArchiveProduct = (reqParams, reqBody) => {
    if(reqBody.isAdmin){
        let update = {
            isActive: true
        }

        return Product.findByIdAndUpdate(reqParams.productId, update).then((res, err) => {
            if(err){
                return `Something went wrong. Please try again later!`
            } else {
                return `Product has been successfully unarchived`
            }
        })
    } else {
        return Promise.resolve(`Unauthorized access`)
    }
}