const Order = require('../models/Orders')
const Product = require('../models/Products')

module.exports.checkOut = (reqParams, reqBody) => {
    if(!reqBody.isAdmin){
        return Product.findOne({_id: reqParams.productId }).then(res => {
            let data = []

            data.push({
                productId: res._id,
                quantity: reqBody.quantity.quantity,
            })

            let newOrder = new Order({
                totalAmount: res.price * reqBody.quantity.quantity,
                userId: reqBody.id,
                products: data
            })

            return newOrder.save().then((res, err) => {
                if(err){
                    return `Something went wrong. Please try again later`
                } else {
                    return `Your order has been recieved`
                }
            })
        })
    } else {
        return Promise.resolve(`Admins can't check out!`)
    }
}

module.exports.getOrders = reqBody => {
    return Order.find({userId: reqBody.id}).then(res => {
        if(res == null){
            'Please login to access this page'
        } else {
            return res
        }
    })
}

module.exports.getAllOrders = reqBody =>{
    if(reqBody){
        return Order.find({}).then(res => {
            if(res.length <= 0){
                return `There's no product as of this moment`
            } else {
                return res
            }
        })
    } else {
        return Promise.resolve(`Unauthorized access`)
    }
}