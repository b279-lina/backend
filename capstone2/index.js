const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoute = require('./routes/user')
const productRoute = require('./routes/product')
const orderRoute = require('./routes/order')
const app = express()

let port = 4000

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.fz6dt1w.mongodb.net/Capstone2_API?retryWrites=true&w=majority',
{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log(`Now connected to MongoDB Atlas`))

app.use('/', userRoute)
app.use('/', productRoute)
app.use('/users', orderRoute)

if(require.main === module){
    app.listen(process.env.PORT || port, () => console.log(`API is now online on port ${process.env.PORT || port}`))
}

module.exports = app