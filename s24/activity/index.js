//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Exponent Operator

// Template Literals

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

// Javascript Classes

// getCube
const getCube = (cubeSize) =>{
    return `The cube of ${cubeSize} is ${Math.pow(cubeSize, 3)}`;
}

console.log(getCube(2));

// Array Destructure
const [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

// Object Destructure
const {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)

// ForEach
numbers.forEach(eachNumber => {
    console.log(eachNumber)
});

// Add all
let reduceNumber = () => numbers.reduce((x, y) => x + y);
let total = reduceNumber()
console.log(total)

class Dog{
    constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
    }
}

const newDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(newDog)

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}