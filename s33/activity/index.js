// GET all data from todos
const getAll = async () => {
    await fetch('https://jsonplaceholder.typicode.com/todos')
    .then(res => res.json())
    .then(mapping => mapping.map(results => {
        return results.title
    }))
    .then(result => console.log(result))
}

getAll()

// GET data where id = ?
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(res => {return res.json()})
.then(result => console.log(`The item ${result.title} on the list has a status of ${result.completed}`))

// POST new data
fetch('https://jsonplaceholder.typicode.com/todos', {
    method: "POST",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({
        userId: 1,
        title: 'Created To Do List Items',
        completed: true,
    })
})
.then(res => res.json())
.then(result => console.log(result))

// UPDATE data
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: "PUT",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({
        dateCompeleted: "Pending",
        description: "To update my to do list with a different data structure",
        id: 1,
        status: "Pending",
        title: "Updated To Do List Item",
        userId: 1
    })
})
.then(res => res.json())
.then(result => console.log(result))

// PATCH data
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: "PATCH",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({
        dateCompleted : "05/03/23",
		status : "Completed"

    })
})
.then(res => res.json())
.then(result => console.log(result))

// DELETE data
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    METHOD: "DELETE"
})
.then(res => res.json())
.then(result => console.log(`Deleted ${result.title} from data base`))